package common

import (
	"net/http"
	"time"

	"gitlab.com/ak1m1tsu/go-libs/contextlib"
)

func New() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := contextlib.SetRequestID(r.Context(), r)
			ctx = contextlib.SetRealIP(ctx, r)
			ctx = contextlib.SetRequestStartTime(ctx, time.Now())

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
