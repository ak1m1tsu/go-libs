package appname

import (
	"net/http"

	"gitlab.com/ak1m1tsu/go-libs/contextlib"
)

func New(name string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := contextlib.SetServer(r.Context(), name)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
