package logger

import (
	"net/http"

	"gitlab.com/ak1m1tsu/go-libs/contextlib"
)

func New(logger Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			logger.Start(ctx, map[string]any{
				"X-Request-Id":         contextlib.GetRequestID(ctx),
				"X-Request-Start-Time": contextlib.GetRequestStartTime(ctx),
				"X-Real-IP":            contextlib.GetRealIP(ctx),
				"X-Server":             contextlib.GetServer(ctx),
				"X-CSRF-Token":         r.Header.Get("X-CSRF-Token"),
				"Content-Type":         r.Header.Get("Content-Type"),
				"Content-Length":       r.Header.Get("Content-Length"),
				"Referer":              r.Header.Get("Referer"),
				"Method":               r.Method,
				"Path":                 r.URL.Path,
				"User-Agent":           r.UserAgent(),
			})
			defer logger.End(ctx, map[string]any{
				"X-Request-Id":         contextlib.GetRequestID(ctx),
				"X-Request-Start-Time": contextlib.GetRequestStartTime(ctx),
				"X-Real-IP":            contextlib.GetRealIP(ctx),
				"X-Server":             contextlib.GetServer(ctx),
				"X-CSRF-Token":         r.Header.Get("X-CSRF-Token"),
				"Content-Type":         r.Header.Get("Content-Type"),
				"Referer":              r.Header.Get("Referer"),
				"Method":               r.Method,
				"Path":                 r.URL.Path,
				"User-Agent":           r.UserAgent(),
				"Status":               0,
			})

			next.ServeHTTP(w, r)
		})
	}
}
