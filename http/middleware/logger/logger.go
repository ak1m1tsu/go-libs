package logger

import "context"

type Logger interface {
	Start(ctx context.Context, params map[string]any)
	End(ctx context.Context, params map[string]any)
}
