package contextlib

import (
	"context"
	"os"
	"os/signal"
)

func WithSignals(ctx context.Context, s ...os.Signal) context.Context {
	ctx, cancal := signal.NotifyContext(ctx, s...)

	go func() {
		defer cancal()
		<-ctx.Done()
	}()

	return ctx
}
