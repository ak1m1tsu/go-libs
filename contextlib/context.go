package contextlib

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/google/uuid"
)

type ctxKey int

const (
	XRequestID ctxKey = iota
	XRequestStartTime
	XServer
	XCSRFToken
	XRealIP
)

func SetRequestID(ctx context.Context, r *http.Request) context.Context {
	reqID := r.Header.Get("X-Request-ID")
	if reqID == "" {
		reqID = uuid.NewString()
	}

	return context.WithValue(ctx, XRequestID, reqID)
}

// GetRequestID returns the request id if it exists
// or the empty string if it doesn't exists
func GetRequestID(ctx context.Context) string {
	reqID, ok := ctx.Value(XRequestID).(string)
	if !ok {
		return ""
	}

	return reqID
}

func SetRequestStartTime(ctx context.Context, reqTime time.Time) context.Context {
	return context.WithValue(ctx, XRequestStartTime, reqTime)
}

// GetRequestStartTime returns the request start time if it exists
// or time.Now() if it doesn't exists
func GetRequestStartTime(ctx context.Context) time.Time {
	reqID, ok := ctx.Value(XRequestStartTime).(time.Time)
	if !ok {
		return time.Now()
	}

	return reqID
}

func SetRealIP(ctx context.Context, r *http.Request) context.Context {
	realIP := r.Header.Get("X-Real-IP")
	if realIP == "" {
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			realIP = "0.0.0.0"
		} else {
			realIP = ip
		}
	}

	return context.WithValue(ctx, XRealIP, realIP)
}

func GetRealIP(ctx context.Context) string {
	realIP, ok := ctx.Value(XRealIP).(string)
	if ok {
		return realIP
	}

	return ""
}

func SetServer(ctx context.Context, server string) context.Context {
	return context.WithValue(ctx, XServer, server)
}

func GetServer(ctx context.Context) string {
	name, ok := ctx.Value(XServer).(string)
	if ok {
		return name
	}

	return ""
}
